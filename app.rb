# encoding: UTF-8

require 'sinatra'
require "sinatra/reloader" if development?
require 'sinatra/activerecord'
require 'sinatra/cross_origin'
require 'active_support/all'
require 'rabl'
require 'oj'
require 'jwt'
require 'warden'
require 'tzinfo'

require 'bundler/setup'
require 'yaml'

require './models/init.rb'

class Clast < Sinatra::Base

	#==================
	# Sinatra settings
	#==================

	#---------------
	# Miscellaneous
	#---------------

	TIMEZONE = "Tokyo"

	configure do
		Oj.default_options = { time_format: :xmlschema }
	end

	configure :development do
		register Sinatra::Reloader
	end

	#--------------
	# ActiveRecord
	#--------------

	configure do

		ActiveRecord::Base.default_timezone = :local

		dbconfig = YAML.load(ERB.new(File.read("config/database.yml")).result)
		RACK_ENV ||= ENV["RACK_ENV"] || "development"
		ActiveRecord::Base.establish_connection dbconfig[RACK_ENV]

		if ENV["OPENSHIFT_RUBY_LOG_DIR"]
			LOG_FILE = "#{ENV["OPENSHIFT_RUBY_LOG_DIR"]}/#{RACK_ENV}.log"
			ActiveRecord::Base.logger = Logger.new(File.open(LOG_FILE, 'a+'))
		else
			Dir.mkdir('log') if !File.exist?('log') || !File.directory?('log')
			ActiveRecord::Base.logger = Logger.new(File.open("log/#{RACK_ENV}.log", "a+"))
		end
	end

	#------
	# RABL
	#------

	Rabl.register!
	Rabl.configure do |config|
		config.include_json_root = false
	end

	#------
	# CORS
	#------

	configure do
		register Sinatra::CrossOrigin
	  enable :cross_origin
		set :allow_methods, [:get, :post, :put, :options]
		set :allow_credentials, true
	end
	configure :production do
		set :allow_origin, ['http://outcrop.azunyan.info']
	end
	configure :development do
		set :allow_origin, :any
		set :allow_methods, [:get, :post, :put, :options]
	end

	# responding to OPTIONS
	# https://github.com/britg/sinatra-cross_origin
	options "*" do
	  response.headers["Allow"] = "HEAD,GET,PUT,POST,DELETE,OPTIONS"
	  response.headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept"
	  200
	end

	#-----
	# JWT
	#-----

	# TODO: configure JWT

	#--------
	# Warden
	#--------

	use Warden::Manager do |config|
	    config.scope_defaults :default, strategies: [:basic], action: '/rejected'
	    config.failure_app = self
	end

	Warden::Manager.before_failure do |env,opts|
	    env['REQUEST_METHOD'] = 'GET'
	end

	# TODO: user authentication
	Warden::Strategies.add(:basic) do
		def valid?
			params["id"].is_a?(String)
			params["password"].is_a?(String)
		end

		def authenticate!
			access_granted = (
				params["id"] == 'hoge' &&
				params["password"] == '123'
			)
			!access_granted ? fail!("Could not log in") : success!(access_granted)
		end
	end

	# TODO: use JWT
	Warden::Strategies.add(:access_token) do
		def valid?
			request.env["HTTP_ACCESS_TOKEN"].is_a?(String)
		end

		def authenticate!
			access_granted = (request.env["HTTP_ACCESS_TOKEN"] == 'hoge')
			!access_granted ? fail!("Could not log in") : success!(access_granted)
		end
	end


	#=======================
	# Common helper methods
	#=======================

	helpers do

		def set_date_range(params)

			begin
				@start_date = Date.parse(params['start'])
				@end_date = Date.parse(params['end'])
			rescue ArgumentError
				return false
			end

			if !@start_date || !@end_date then
				return false
			end

			return true
		end
	end


	#=========
	# Routing
	#=========

	get '/' do
		"working"
	end


	# TODO: remove
	get '/login' do
		return "<html><body><form method='post'>id: <input name='id' type='text'><br>password: <input name='password' type='text'><br><button type='submit'>login</button></form></body></html>"
	end
	post '/login' do
		#env['warden'].authenticate!(:access_token)
		env['warden'].authenticate!(:basic)
		return "ろぐいん せいこう"
	end
	get '/rejected' do
		return "ろぐいん きょぜつ"
	end


	#------
	# User
	#------

	get '/users' do
		@users = User.all
		rabl :users, :format => "json"
	end
	put '/users' do
		request.body.rewind
		@users = Oj.load(request.body.read)

		begin
			ActiveRecord::Base.transaction do
				@users.each do |newer_user|
					User.find_or_initialize_by(id: user["id"]).update_attributes!(
						name: newer_user["name"],
						email: newer_user["email"]
					)
				end
			end
			p "success" # TODO: return json
		rescue => e
			p "error: " + e.message # TODO: return json
		end
	end


	#---------------
	# AttendanceLog
	#---------------

	helpers do
		def get_attendance_logs()
			return AttendanceLog.joins(
				:calendar_date,
				:attendance_type
			).select('
				calendar_dates.date,
				attendance_type_id,
				start_time,
				end_time,
				attendance_types.is_time_required,
				calendar_dates.is_holiday
			').order('calendar_dates.date')
		end

		def render_attendance_logs()
			rabl :attendance_logs, :format => "json"
		end
	end
	get '/attendance_logs' do
		if set_date_range(params) then
			@attendance_logs = get_attendance_logs().where(calendar_dates: {date: @start_date..@end_date})
			render_attendance_logs()
		end
	end
	get '/attendance_logs/:year' do
		@attendance_logs = get_attendance_logs().where(calendar_dates: {year: params['year']})
		render_attendance_logs()
	end
	get '/attendance_logs/:year/:month' do
		@attendance_logs = get_attendance_logs().where(calendar_dates: {year: params['year'], month: params['month']})
		render_attendance_logs()
	end
	get '/attendance_logs/:year/:month/:day' do
		@attendance_logs = get_attendance_logs().where(calendar_dates: {year: params['year'], month: params['month'], day: params['day']})
		render_attendance_logs()
	end
	put '/attendance_logs' do
		request.body.rewind
		@attendance_logs = Oj.load(request.body.read)
		begin
			ActiveRecord::Base.transaction do
				@attendance_logs.each do |newer_log|

					Time.zone = TIMEZONE
					date = Time.zone.parse(newer_log["date"]).to_date
					calendar = CalendarDate.find_or_create_by!(date: date)

					AttendanceLog
						.find_or_initialize_by(calendar_date_id: calendar.id)
						.update_attributes!(
							attendance_type_id: newer_log["attendanceTypeId"],
							start_time: newer_log["startTime"],
							end_time: newer_log["endTime"]
						)
				end
			end
			p "success" # TODO: return json
		rescue => e
			p "error: " + e.message # TODO: return json
		end
	end


	#----------------
	# AttendanceType
	#----------------

	get '/attendance_types' do
		@attendance_types = AttendanceType.all.order(:id)
		rabl :attendance_types, :format => "json"
	end


	#--------------
	# CalendarDate
	#--------------

	helpers do
		def get_calendar_dates()
			return CalendarDate.select('
				date,
				is_holiday
			').order(:date)
		end

		def render_calendar_dates()
			rabl :calendar_dates, :format => "json"
		end
	end
	get '/calendar_dates' do
		if set_date_range(params) then
			@calendar_dates = get_calendar_dates().where(date: @start_date..@end_date)
			render_calendar_dates()
		end
	end
	get '/calendar_dates/:year' do
		@calendar_dates = get_calendar_dates().where(year: params['year'])
		render_calendar_dates()
	end
	get '/calendar_dates/:year/:month' do
		@calendar_dates = get_calendar_dates().where(year: params['year'], month: params['month'])
		render_calendar_dates()
	end
	get '/calendar_dates/:year/:month/:day' do
		@calendar_dates = get_calendar_dates().where(year: params['year'], month: params['month'], day: params['day'])
		render_calendar_dates()
	end

end # class Clast
