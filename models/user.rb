# encoding: UTF-8

class User < ActiveRecord::Base
  has_many :attendance_logs
end
