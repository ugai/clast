# encoding: UTF-8

class CalendarDate < ActiveRecord::Base
  has_many :attendance_logs
  after_initialize :init

  def init
    return if !new_record?
    # set default values
    self.year       ||= self.date.year
    self.month      ||= self.date.month
    self.day        ||= self.date.day
    self.quarter    ||= ((self.date.month)/3.0).ceil
    self.week       ||= self.date.cweek
    self.is_holiday ||= (self.date.wday == 0 || self.date.wday == 6)
  end
end
