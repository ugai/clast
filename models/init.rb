# encoding: UTF-8

Dir[File.dirname(__FILE__) + '/*.rb'].each do |file|

  # to avoid circular dependencies
  if __FILE__ == file then
    next
  end

  require file

end
