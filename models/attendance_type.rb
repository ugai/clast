# encoding: UTF-8

class AttendanceType < ActiveRecord::Base
  has_many :attendance_logs
end
