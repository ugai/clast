# encoding: UTF-8

class AttendanceLog < ActiveRecord::Base
  belongs_to :attendance_type
  belongs_to :user
  belongs_to :calendar_date

  DELIMITER = ':'

  # override getters
  def start_time
    to_time_string(super)
  end
  def end_time
    to_time_string(super)
  end

  # override setters
  def start_time=(time_string)
    super(to_minutes(time_string))
  end
  def end_time=(time_string)
    super(to_minutes(time_string))
  end

  # convertion from 90 to "01:30".
  def to_time_string(minutes)
    return "" unless minutes.present?
    hours = minutes / 60
    minutes = minutes % 60
    time_string = "%02d%s%02d" % [hours, DELIMITER, minutes]
    return time_string
  end
  # convertion from "01:30" to 90.
  def to_minutes(time_string)
    return nil unless time_string.present?
    timefields = time_string.split(DELIMITER).map(&:to_i)
    minutes = (timefields[0] * 60) + timefields[1]
    return minutes
  end

end
