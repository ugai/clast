collection @calendar_dates
attributes :date
attributes :is_holiday => :isHoliday
node(:isPast) { |d| d.date < Date.today() }
node(:isToday) { |d| d.date == Date.today() }
node(:isFuture) { |d| d.date > Date.today() }
