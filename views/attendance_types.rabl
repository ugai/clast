collection @attendance_types
attributes :id, :name
attributes :for_holiday => :forHoliday
attributes :is_time_required => :isTimeRequired
