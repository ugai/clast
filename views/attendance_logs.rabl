collection @attendance_logs
attributes :date
attributes :attendance_type_id => :attendanceTypeId
attributes :start_time => :startTime
attributes :end_time => :endTime
attributes :is_time_required => :isTimeRequired
attributes :for_holiday => :forHoliday
