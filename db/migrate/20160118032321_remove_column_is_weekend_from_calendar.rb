class RemoveColumnIsWeekendFromCalendar < ActiveRecord::Migration
  def change
    remove_column :calendars, :is_weekend
  end
end
