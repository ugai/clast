class RenameCalendarDate < ActiveRecord::Migration
  def change
    rename_column :calendars, :db_date, :date
    rename_table :calendars, :calendar_dates
  end
end
