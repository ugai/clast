class Init < ActiveRecord::Migration
  def change

    create_table :attendance_logs, force: :cascade do |t|
      t.date     :date, null: false
      t.integer  :attendance_type_id
      t.time     :start_time
      t.time     :end_time
      t.timestamps null: false
    end

    create_table :attendance_types, force: :cascade do |t|
      t.string   :name, limit: 80, null: false
      t.timestamps null: false
    end

    create_table :calendars, force: :cascade do |t|
      t.date     :db_date, null: false
      t.integer  :year, limit: 4, null: false
      t.integer  :month, limit: 2, null: false
      t.integer  :day, limit: 2, null: false
      t.integer  :quarter, limit: 1, null: false
      t.integer  :week, limit: 2, null: false
      t.boolean  :is_holiday, default: false, null: false
      t.boolean  :is_weekend, default: false, null: false
      t.timestamps null: false
    end
    add_index :calendars, [:db_date], unique: true
    add_index :calendars, [:year, :month, :day], unique: true

    create_table :projects, force: :cascade do |t|
      t.string   :code, limit: 6, null: false
      t.string   :name, limit: 200, null: false
      t.timestamps null: false
    end
    add_index :projects, [:code], unique: true

    create_table :spent_time_logs, force: :cascade do |t|
      t.integer  :user_id, null: false
      t.integer  :task_id, null: false
      t.date     :date, null: false
      t.time     :spent_time, null: false
      t.timestamps null: false
    end
    add_index :spent_time_logs, [:user_id, :task_id, :date], unique: true

    create_table :tasks, force: :cascade do |t|
      t.integer  :project_id, null: false
      t.string   :code, limit: 4, null: false
      t.string   :name, limit: 100, null: false
      t.timestamps null: false
    end
    add_index :tasks, [:project_id, :code], unique: true

    create_table :users, force: :cascade do |t|
      t.string   :name, limit: 80, null: false
      t.string   :email, limit: 200, null: false
      t.string   :password, limit: 96
      t.timestamps null: false
    end

  end
end
