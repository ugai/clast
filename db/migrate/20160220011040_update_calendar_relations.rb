class UpdateCalendarRelations < ActiveRecord::Migration
  def change
    remove_column :attendance_logs, :date
    remove_column :spent_time_logs, :date
    add_column :attendance_logs, :calendar_date_id, :integer
    add_column :spent_time_logs, :calendar_date_id, :integer
    change_column :attendance_logs, :calendar_date_id, :integer, :null => false
    change_column :spent_time_logs, :calendar_date_id, :integer, :null => false
  end
end
