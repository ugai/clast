class AddUserIdColumnToAttendaceLog < ActiveRecord::Migration
  def change
    add_column :attendance_logs, :user_id, :integer, :first => true
  end
end
