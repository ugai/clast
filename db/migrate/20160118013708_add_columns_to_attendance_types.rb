class AddColumnsToAttendanceTypes < ActiveRecord::Migration
  def change
    add_column :attendance_types, :for_holiday, :boolean, default:false, null: false, :after => :name
    add_column :attendance_types, :is_time_required, :boolean, default:false, null: false, :after => :for_holiday
  end
end
