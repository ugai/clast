class ChangeTimeToInteger < ActiveRecord::Migration
  def change
    change_column :attendance_logs, :start_time, :integer
    change_column :attendance_logs, :end_time, :integer
  end
end
