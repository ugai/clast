# encoding: UTF-8
# This will be run during `rake db:seed` in the :production environment.

include Sprig::Helpers

sprig [
  AttendanceType,
  User
]
