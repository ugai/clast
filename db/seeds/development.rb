# encoding: UTF-8
# This will be run during `rake db:seed` in the :development environment.

include Sprig::Helpers

sprig [
  AttendanceType,
  User
]
