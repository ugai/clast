# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160320040656) do

  create_table "attendance_logs", force: :cascade do |t|
    t.integer  "attendance_type_id"
    t.integer  "start_time"
    t.integer  "end_time"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "user_id"
    t.integer  "calendar_date_id",   null: false
  end

  create_table "attendance_types", force: :cascade do |t|
    t.string   "name",             limit: 80, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "for_holiday"
    t.boolean  "is_time_required"
  end

  create_table "calendar_dates", force: :cascade do |t|
    t.date     "date",                                 null: false
    t.integer  "year",       limit: 4,                 null: false
    t.integer  "month",      limit: 2,                 null: false
    t.integer  "day",        limit: 2,                 null: false
    t.integer  "quarter",    limit: 1,                 null: false
    t.integer  "week",       limit: 2,                 null: false
    t.boolean  "is_holiday",           default: false, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "calendar_dates", ["date"], name: "index_calendar_dates_on_date", unique: true
  add_index "calendar_dates", ["year", "month", "day"], name: "index_calendar_dates_on_year_and_month_and_day", unique: true

  create_table "projects", force: :cascade do |t|
    t.string   "code",       limit: 6,   null: false
    t.string   "name",       limit: 200, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "projects", ["code"], name: "index_projects_on_code", unique: true

  create_table "spent_time_logs", force: :cascade do |t|
    t.integer  "user_id",          null: false
    t.integer  "task_id",          null: false
    t.time     "spent_time",       null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "calendar_date_id", null: false
  end

  add_index "spent_time_logs", ["user_id", "task_id"], name: "index_spent_time_logs_on_user_id_and_task_id_and_date", unique: true

  create_table "tasks", force: :cascade do |t|
    t.integer  "project_id",             null: false
    t.string   "code",       limit: 4,   null: false
    t.string   "name",       limit: 100, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "tasks", ["project_id", "code"], name: "index_tasks_on_project_id_and_code", unique: true

  create_table "users", force: :cascade do |t|
    t.string   "name",       limit: 80,  null: false
    t.string   "email",      limit: 200, null: false
    t.string   "password",   limit: 96
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
