# encoding: UTF-8

require 'sprig'
require 'sinatra'
require 'sinatra/activerecord'
require './models/init.rb'

set :database_file, "config/database.yml"

#-------------
# Sprig seeds
#-------------

# Add support for non-Rails environments · Issue #45 · vigetlabs/sprig
# https://github.com/vigetlabs/sprig/issues/45
# > To use Sprig with Sinatra, I had to monkeypatch Sprig's configuration class as it relies on Rails.root.
path = File.join(Rack::Directory.new('').root, 'db/seeds', "#{ENV['RACK_ENV'] || 'development'}.rb")

module Sprig
  class Configuration
    def directory
      File.join(@directory || default_directory, ENV['RACK_ENV'] || 'development')
    end
  end
  class Source
    class SourceDeterminer
      def filepath
        #path = seed_directory.join(filename)
        path = File.join(seed_directory, filename)
        if File.symlink?(path)
          File.readlink(path)
        else
          path
        end
      end
    end
  end
end

load path if File.exist?(path)


#--------------
# manual seeds
#--------------

(Date.new(Date.today.year-1, 01, 01)..Date.new(Date.today.year+1, 12, 31)).each do |date|
  CalendarDate.create(date: date)
end

# TODO: AttendanceLog
